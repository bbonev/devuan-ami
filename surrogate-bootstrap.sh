#!/bin/bash
# This script is run by packer on the EC2 instance it built and launched to build the AMI
apt-get update

# Installing debootstrap to deploy Devuan packages on additionnal /dev/xvdf Disk
DEBIAN_FRONTEND=noninteractive apt-get install -y debootstrap

# The rootdev device is the one which will receive all the Devuan packages,
# and be used by packer to build the destination Devuan AMI
rootdev=/dev/xvdf

# The mount point where the script will mount / and /boot FSs based on rootdev
mntpoint=/devuan

# Creating a GPT partitions table with a boot partition, /boot and /
sgdisk -og ${rootdev}
sgdisk -n 128:1MiB:2MiB -c 128:"BIOS Boot Partition" -t 128:ef02 ${rootdev}
sgdisk -n 1:3MiB:503MiB -c 1:"Linux Boot" -t 1:8300 ${rootdev}
sgdisk -n 2:504MiB:0 -c 2:"Linux Root" -t 2:8e00 ${rootdev}

# Formatting both partitions
mkfs.ext4 ${rootdev}1
mkfs.ext4 ${rootdev}2

# Mounting partitions
mkdir ${mntpoint}
mount ${rootdev}2 ${mntpoint}
mkdir ${mntpoint}/boot
mount ${rootdev}1 ${mntpoint}/boot

# The file used by debootstrap for Devuan conf is not present in the debootstrap Debian package, so we copy it
# (I manually took it from Devuan package but wwe should have a cleaner way of extracting/getting it)
cp ~/ceres /usr/share/debootstrap/scripts/chimaera

# debootstrap will copy/install all Devuan packages now
debootstrap --arch amd64 chimaera ${mntpoint} http://deb.devuan.org/merged

# Mounting necessary FSs for chroot
for i in {proc,sys,dev,dev/pts,run}
do
  mount --bind /${i} ${mntpoint}/${i}
done

# Copying the script which will be run chrooted, launch it, delete it
chmod 755 chroot-bootstrap.sh && cp chroot-bootstrap.sh ${mntpoint}/tmp
chroot ${mntpoint} /tmp/chroot-bootstrap.sh
rm ${mntpoint}/tmp/chroot-bootstrap.sh

# unmounting FSs used by chroot
for i in {proc,sys,dev/pts,dev,run}
do
  umount ${mntpoint}/${i}
done

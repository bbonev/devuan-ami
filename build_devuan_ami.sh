#!/bin/bash
#
# Validate the json config file
packer validate devuan.json
# Build the AMI based on json config file
packer build devuan.json
